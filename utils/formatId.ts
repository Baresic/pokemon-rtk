export const formatId = (id?: number) => {
  if (id && id < 10) {
    return `#00${id}`;
  }
  return `#0${id}`;
};
