export const getPokemonImage = (name: string) => {
  return require(`../assets/pokemons/${name}.png`);
};
