import React from "react";
import { Provider } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MenuProvider } from "react-native-popup-menu";
import { useFonts } from "expo-font";
import { store } from "./store/store/store";
import { theme } from "./consts";
import {
  AccountScreen,
  DetailsScreen,
  HomeScreen,
  WishlistScreen,
} from "./screens";
import { Entypo } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  HomeScreenStackNavigatorParamList,
  HomeStackNavigatorParamList,
} from "./types";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Tab = createBottomTabNavigator<HomeStackNavigatorParamList>();

const Drawer = createDrawerNavigator();

const HomeStack =
  createNativeStackNavigator<HomeScreenStackNavigatorParamList>();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator
      screenOptions={() => ({
        headerShown: false,
      })}
    >
      <HomeStack.Screen name="HomeScreen" component={HomeScreen} />
      <HomeStack.Screen name="DetailsScreen" component={DetailsScreen} />
    </HomeStack.Navigator>
  );
};

const TabNavigator: React.FC<any> = (props) => {
  const { extraData } = props;
  const screenOptions = (route: any, color: string) => {
    let iconName;

    switch (route.name) {
      case "Home":
        iconName = "home";
        break;
      case "Wishlist":
        iconName = "heart";
        break;
      default:
        break;
    }

    if (route.name === "Account") {
      return <MaterialCommunityIcons name="account" size={24} color={color} />;
    }

    return (
      <Entypo
        name={iconName as keyof typeof Entypo.glyphMap}
        color={color}
        size={24}
      />
    );
  };
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => screenOptions(route, color),
        tabBarLabelPosition: "below-icon",
        tabBarAccessibilityLabel: "bottom-navigation",
        tabBarActiveTintColor: theme.colors.active,
        tabBarInactiveTintColor: theme.colors.navigation_text,
        tabBarStyle: {
          height: 64,
        },
        tabBarItemStyle: {
          margin: 10,
        },
        headerShown: false,
      })}
      initialRouteName={extraData?.name}
    >
      <Tab.Screen name="Home" component={HomeStackScreen} />
      <Tab.Screen name="Wishlist" component={WishlistScreen} />
      <Tab.Screen name="Account" component={AccountScreen} />
    </Tab.Navigator>
  );
};

const DrawerNavigator = () => {
  return (
    <>
      <Drawer.Navigator
        screenOptions={({ navigation, route }) => ({
          drawerPosition: "right",
          headerShown: false,
          headerTransparent: true,
          headerStyle: {
            backgroundColor: theme.colors.text_white,
            borderBottomColor: theme.colors.text_white,
          },
        })}
      >
        <Drawer.Screen name="Home Page">
          {(props) => <TabNavigator {...props} extraData={{ name: "Home" }} />}
        </Drawer.Screen>

        <Drawer.Screen name="Wishlist Page">
          {(props) => (
            <TabNavigator {...props} extraData={{ name: "Wishlist" }} />
          )}
        </Drawer.Screen>

        <Drawer.Screen name="Account Page">
          {(props) => (
            <TabNavigator {...props} extraData={{ name: "Account" }} />
          )}
        </Drawer.Screen>
      </Drawer.Navigator>
    </>
  );
};

export default function App() {
  const [loaded] = useFonts({
    Flexo: require("./assets/fonts/Flexo-Demi.ttf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <Provider store={store}>
      <MenuProvider>
        <NavigationContainer>
          <DrawerNavigator />
        </NavigationContainer>
      </MenuProvider>
    </Provider>
  );
}
