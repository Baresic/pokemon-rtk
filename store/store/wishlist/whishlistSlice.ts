import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { PokemonDetails } from "../../../api";

interface WishlistState {
  pokemons: Array<PokemonDetails>;
}

const initialState: WishlistState = {
  pokemons: [],
};

export const whishlistSlice = createSlice({
  name: "wishlist",
  initialState,
  reducers: {
    pokemnsAdded(state, action: PayloadAction<PokemonDetails>) {
      state.pokemons.push(action.payload);
    },
    pokemonsDeleted(state, action: PayloadAction<string>) {
      state.pokemons = state.pokemons.filter(
        (pokemon: PokemonDetails) => pokemon.name !== action.payload
      );
    },
  },
});

export const { pokemnsAdded, pokemonsDeleted } = whishlistSlice.actions;
