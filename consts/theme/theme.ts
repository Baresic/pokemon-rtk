export const palette = {
  white: "#FFFFFF",
  smoke: "rgba(245, 243, 246, 0.3)",
  black: "#35353A",
  purple: "#7076C9",
  gray: "#757575",
  green: "rgb(112, 205, 177)",
  green_reduced: "rgba(165, 237, 212, 1)",
  rose: "rgba(234, 116, 111, 1)",
  rose_reduced: "rgba(255, 162, 161, 1)",
  blue: "rgba(138, 188, 245, 1)",
  blue_reduced: "rgba(188, 215, 255, 1)",
  yellow: "rgba(245, 218, 130, 1)",
  yellow_reduced: "rgba(255, 245, 180, 1)",
  marker_rose: "#DD6571",
  marker_green: "#7FC99B",
  search_gray: "#F2F2F2",
  image_back: "#A5EDD4",
  back: "#F5F3F6",
};

export const colorObjects = [
  {
    name: "green",
    value: palette.green,
    bg: palette.green_reduced,
  },
  {
    name: "green",
    value: palette.green,
    bg: palette.green_reduced,
  },
  {
    name: "green",
    value: palette.green,
    bg: palette.green_reduced,
  },
  {
    name: "rose",
    value: palette.rose,
    bg: palette.rose_reduced,
  },
  {
    name: "rose",
    value: palette.rose,
    bg: palette.rose_reduced,
  },
  {
    name: "rose",
    value: palette.rose,
    bg: palette.rose_reduced,
  },
  {
    name: "blue",
    value: palette.blue,
    bg: palette.blue_reduced,
  },
  {
    name: "blue",
    value: palette.blue,
    bg: palette.blue_reduced,
  },
  {
    name: "blue",
    value: palette.blue,
    bg: palette.blue_reduced,
  },
  {
    name: "yellow",
    value: palette.yellow,
    bg: palette.yellow_reduced,
  },
  {
    name: "yellow",
    value: palette.yellow,
    bg: palette.yellow_reduced,
  },
  {
    name: "yellow",
    value: palette.yellow,
    bg: palette.yellow_reduced,
  },
  {
    name: "green",
    value: palette.green,
    bg: palette.green_reduced,
  },
  {
    name: "green",
    value: palette.green,
    bg: palette.green_reduced,
  },
  {
    name: "green",
    value: palette.green,
    bg: palette.green_reduced,
  },
  {
    name: "rose",
    value: palette.rose,
    bg: palette.rose_reduced,
  },
  {
    name: "rose",
    value: palette.rose,
    bg: palette.rose_reduced,
  },
  {
    name: "rose",
    value: palette.rose,
    bg: palette.rose_reduced,
  },
  {
    name: "blue",
    value: palette.blue,
    bg: palette.blue_reduced,
  },
  {
    name: "blue",
    value: palette.blue,
    bg: palette.blue_reduced,
  },
];

export const theme = {
  colors: {
    text: palette.black,
    text_white: palette.white,
    navigation_text: palette.gray,
    active: palette.purple,
    back: palette.smoke,
    markerGreen: palette.marker_green,
    markerRose: palette.marker_rose,
    background: palette.back,
  },
  spacing: {
    s: 8,
    m: 16,
    l: 24,
    xl: 40,
  },
  borderRadius: {
    s: 12,
    m: 20,
    xl: 200,
  },
  fontSize: {
    s: 12,
    m: 16,
    xl: 28,
  },
  textVariants: {
    header_card: {
      fontFamily: "Flexo",
      fontSize: 16,
      lineHeight: 19,
      letterSpacing: 0,
    },
    pill: {
      fontFamily: "Flexo",
      fontSize: 12,
      lineHeight: 15,
      letterSpacing: 0,
      textTransform: "capitalize",
    },
    title_pokemon_details: {
      fontFamily: "Flexo",
      fontSize: 28,
      lineHeight: 34,
      letterSpacing: 0,
    },
    text_pokemon_details: {
      fontFamily: "Flexo",
      fontSize: 16,
      lineHeight: 19,
      letterSpacing: 0,
      textAlign: "left",
      opacity: 0.5,
    },
    body: {
      fontFamily: "Flexo",
      fontSize: 16,
    },
  },
};
