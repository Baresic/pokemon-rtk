import React from "react";
import { View, StyleSheet } from "react-native";
import { Bar as ProgressBar } from "react-native-progress";
import { theme } from "../../consts";

type ProgressBarProps = {
  value: number;
};

export const Bar: React.FC<ProgressBarProps> = ({ value }) => {
  const processValue = () => {
    if (Math.floor(value * 10) / 1000 > 1) {
      return 1;
    }
    return Math.floor(value * 10) / 1000;
  };

  const processColor = () => {
    if (Math.floor(value * 10) / 1000 > 0.5) {
      return theme.colors.markerGreen;
    }
    return theme.colors.markerRose;
  };
  return (
    <View style={styles.container}>
      <ProgressBar
        progress={processValue()}
        color={processColor()}
        borderWidth={0}
        unfilledColor={theme.colors.back}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexGrow: 1,
  },
});
