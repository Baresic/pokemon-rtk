import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { PokemonDetails } from "../../api";
import { theme } from "../../consts";

type AboutRowProps = {
  data: PokemonDetails;
};

export const AboutRow: React.FC<AboutRowProps> = ({ data }) => {
  return (
    <View style={styles.container}>
      <View style={styles.rowWrap}>
        <Text style={styles.info}>Name: {data?.name}</Text>
        <Text style={styles.info}>Height: {data?.height}</Text>
        <Text style={styles.info}>Weight: {data?.weight}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    paddingTop: 20,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  rowWrap: {
    width: "100%",
  },
  info: {
    color: theme.colors.text,
    opacity: 0.5,
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    lineHeight: 19,
    textTransform: "capitalize",
    marginBottom: 18,
  },
});
