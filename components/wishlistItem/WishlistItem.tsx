import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import { PokemonDetails } from "../../api";
import { pokemonsDeleted } from "../../store/store/wishlist";
import { useDispatch } from "react-redux";
import { theme } from "../../consts";
import { formatId, getPokemonImage } from "../../utils";
import { Pill } from "../pill";
import { useSelector } from "react-redux";
import { RootState } from "../../store";
import { Entypo } from "@expo/vector-icons";

type WishlistItemProps = {
  pokemon: PokemonDetails;
};

export const WishlistItem: React.FC<WishlistItemProps> = ({ pokemon }) => {
  const dispatch = useDispatch();

  const wishlistItems = useSelector(
    (state: RootState) => state.wishlist.pokemons
  );
  return (
    <View style={{ ...styles.itemWrap, backgroundColor: pokemon.bgColor }}>
      <View style={styles.firstCol}>
        <Text style={styles.id}>{formatId(pokemon?.id)}</Text>
        <Text style={styles.cardTitle}>{pokemon?.name}</Text>
        <View style={{ display: "flex", flexDirection: "row" }}>
          {pokemon?.types?.map((detail, i) => (
            <Pill key={i} type={detail?.type} isMargin />
          ))}
        </View>
      </View>

      <View>
        <Entypo
          style={styles.wishlistIcon}
          name={
            wishlistItems.some((item) => item.name === pokemon?.name)
              ? "heart"
              : "heart-outlined"
          }
          color="white"
          size={24}
          onPress={() => {
            dispatch(pokemonsDeleted(pokemon?.name as string));
          }}
        />
        <View style={styles.imageWrap}>
          <Image style={styles.image} source={getPokemonImage(pokemon?.name)} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  itemWrap: {
    maxWidth: 392,
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    height: 140,
    borderRadius: theme.borderRadius.m,
    paddingTop: theme.spacing.l,
    paddingLeft: 20,
    display: "flex",
    flexDirection: "row",
    overflow: "hidden",
    position: "relative",
    marginBottom: 16,
  },
  cardTitle: {
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: 24,
    letterSpacing: 0,
    marginBottom: theme.spacing.s,
    color: theme.colors.text_white,
    textTransform: "capitalize",
  },
  id: {
    fontFamily: theme.textVariants.body.fontFamily,
    opacity: 0.4,
    marginBottom: theme.spacing.s,
  },
  firstCol: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
  },
  textWrap: {
    display: "flex",
    flexDirection: "column",
    marginRight: theme.spacing.s,
  },
  imageWrap: {
    width: 150,
    height: 150,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: theme.borderRadius.xl,
    backgroundColor: theme.colors.back,
    position: "absolute",
    right: 0,
    top: -28,
  },
  wishlistIcon: {
    marginRight: 16,
    marginTop: -5,
    zIndex: 1,
  },
  image: {
    width: 88,
    height: 80,
  },
});
