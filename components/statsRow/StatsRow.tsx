import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { PokemonStats } from "../../api";
import { theme } from "../../consts";
import { Bar } from "../bar/Bar";

type StartRowProps = {
  data: PokemonStats[];
};

export const StatsRow: React.FC<StartRowProps> = ({ data }) => {
  const formatName = (name?: string) => {
    if (name === "special-attack") {
      return "Sp. Atk";
    }
    if (name === "hp") {
      return "HP";
    }
    if (name === "special-defense") {
      return "Sp. Def";
    }
    return name;
  };
  const totalValue = (data: PokemonStats[]) => {
    return data?.reduce((total, obj) => total + obj.base_stat, 0);
  };
  return (
    <View style={styles.container}>
      {data.map((stat) => {
        return (
          <View style={styles.rowWrap}>
            <Text style={styles.name}>{formatName(stat?.stat?.name)}</Text>
            <Text style={styles.base}>{stat?.base_stat}</Text>
            <Bar value={stat?.base_stat} />
          </View>
        );
      })}
      <View style={styles.rowWrap}>
        <Text style={styles.name}>Total</Text>
        <Text style={styles.base}>{totalValue(data)}</Text>
        <Bar value={60} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    paddingTop: 20,
  },
  rowWrap: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    marginBottom: 18,
  },
  name: {
    minWidth: 100,
    color: theme.colors.text,
    opacity: 0.5,
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    lineHeight: 19,
    textTransform: "capitalize",
  },
  base: {
    minWidth: 37,
    color: theme.colors.text,
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    lineHeight: 19,
  },
});
