import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import { PokemonType } from "../../api";
import { theme } from "../../consts";

type PillProps = {
  type: PokemonType;
  isMargin?: boolean;
};

export const Pill: React.FC<PillProps> = ({ type, isMargin }) => {
  return (
    <View
      style={{
        ...styles.background,
        marginRight: isMargin ? 6 : 0,
      }}
    >
      <Text style={styles.text}>{type?.name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    marginBottom: theme.spacing.s,
    backgroundColor: "rgba(255,255,255, 0.5)",
    borderRadius: theme.borderRadius.s,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minWidth: 62,
    maxWidth: 69,
  },
  text: {
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.fontSize.s,
    color: theme.colors.text_white,
    lineHeight: 15,
    letterSpacing: 0,
    textTransform: "capitalize",
    textAlign: "left",
    marginTop: 5,
    marginBottom: 4,
  },
});
