import { Ionicons } from "@expo/vector-icons";
import { DrawerActions } from "@react-navigation/native";
import React from "react";
import { View, StyleSheet, ImageBackground } from "react-native";

type HamburgerProps = {
  navigation: any;
};

export const Hamburger: React.FC<HamburgerProps> = ({ navigation }) => {
  return (
    <View style={styles.wrap}>
      <ImageBackground
        source={require("../../assets/back.png")}
        resizeMode="cover"
        style={styles.back}
      />
      <Ionicons
        name="menu-sharp"
        size={24}
        color="black"
        onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}
        style={styles.icon}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  wrap: {
    position: "absolute",
    top: 0,
    right: 0,
    zIndex: 1,
  },
  back: {
    width: 137,
    height: 137,
    position: "absolute",
    top: -36,
    right: -40,
    zIndex: 0,
  },
  icon: {
    position: "absolute",
    zIndex: 100,
    paddingRight: 16,
    top: 20,
    right: 0,
  },
});
