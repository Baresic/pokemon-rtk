import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { PokemonMoves } from "../../api";
import { theme } from "../../consts";

type MovesRowProps = {
  data: PokemonMoves[];
};

export const MovesRow: React.FC<MovesRowProps> = ({ data }) => {
  return (
    <View style={styles.container}>
      {data.map((move) => {
        return (
          <View style={styles.rowWrap}>
            <Text style={styles.name}>{move?.move?.name}</Text>
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    paddingTop: 20,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  rowWrap: {
    width: "100%",
    marginBottom: 18,
  },
  name: {
    color: theme.colors.text,
    opacity: 0.5,
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    lineHeight: 19,
    textTransform: "capitalize",
  },
});
