import { View, StyleSheet, Image } from "react-native";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";
import { theme } from "../../consts";

type FilterMenuProps = {
  filterBy: (text: string) => void;
  clearFilters: () => void;
};

export const FilterMenu: React.FC<FilterMenuProps> = ({
  filterBy,
  clearFilters,
}) => (
  <View>
    <Menu>
      <MenuTrigger>
        <Image
          style={styles.image}
          source={require("../../assets/icons/filter.png")}
        />
      </MenuTrigger>
      <MenuOptions customStyles={{ optionsContainer: { ...styles.wrap } }}>
        <MenuOption
          customStyles={{
            optionText: {
              ...styles.text,
            },
          }}
          onSelect={() => filterBy(`fire`)}
          text="Filter fire type"
        />
        <MenuOption
          customStyles={{
            optionText: {
              ...styles.text,
            },
          }}
          onSelect={() => filterBy(`water`)}
          text="Filter water type"
        />
        <MenuOption
          customStyles={{
            optionText: {
              ...styles.text,
            },
          }}
          onSelect={() => clearFilters()}
          text="Clear all"
        />
      </MenuOptions>
    </Menu>
  </View>
);

const styles = StyleSheet.create({
  image: {
    width: 24,
    height: 24,
  },
  wrap: {
    padding: 8,
  },
  text: {
    marginBottom: theme.spacing.s,
    color: theme.colors.text,
    fontSize: theme.textVariants.body.fontSize,
    fontFamily: theme.textVariants.body.fontFamily,
  },
});
