import React from "react";
import { View, StyleSheet, Text, ActivityIndicator } from "react-native";
import { PokemonSpieces, useGetPokemonSpiecesQuery } from "../../api";
import { theme } from "../../consts";

type EvolutionRowProps = {
  species: PokemonSpieces;
};

export const EvolutionRow: React.FC<EvolutionRowProps> = ({ species }) => {
  const { data, isLoading } = useGetPokemonSpiecesQuery(species?.name);

  const renderResult = () => {
    if (data?.evolves_from_species !== null) {
      return `Evolves from species: ${data?.evolves_from_species?.name}`;
    }
    return "Evolves from species: Not specified";
  };
  if (isLoading) {
    return <ActivityIndicator />;
  }
  return (
    <View style={styles.container}>
      <View style={styles.rowWrap}>
        <Text style={styles.name}>{renderResult()}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 16,
    paddingTop: 20,
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  rowWrap: {
    width: "100%",
    marginBottom: 18,
  },
  name: {
    color: theme.colors.text,
    opacity: 0.5,
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    lineHeight: 19,
    textTransform: "capitalize",
  },
});
