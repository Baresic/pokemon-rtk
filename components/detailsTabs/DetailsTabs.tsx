import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { PokemonDetails } from "../../api";
import { theme } from "../../consts";
import { AboutRow } from "../aboutRow/AboutRow";
import { EvolutionRow } from "../evolutionRow/EvolutionRow";
import { MovesRow } from "../movesRow/MovesRow";
import { StatsRow } from "../statsRow/StatsRow";

type DetailsTabProps = {
  data: PokemonDetails;
};

export const DetailsTabs: React.FC<DetailsTabProps> = ({ data }) => {
  const [index, setIndex] = useState(0);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: theme.colors.text_white,
        zIndex: 2,
        paddingTop: 63,
        marginTop: -63,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
      }}
    >
      <View style={styles.tabStatusBar}>
        <TouchableOpacity onPress={() => setIndex(0)}>
          <Text
            style={
              index === 0 ? styles.tabBarLabelUnderline : styles.tabBarLabel
            }
          >
            About
          </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => setIndex(1)}>
          <Text
            style={
              index === 1 ? styles.tabBarLabelUnderline : styles.tabBarLabel
            }
          >
            Base stats
          </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => setIndex(2)}>
          <Text
            style={
              index === 2 ? styles.tabBarLabelUnderline : styles.tabBarLabel
            }
          >
            Evolution
          </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => setIndex(3)}>
          <Text
            style={
              index === 3 ? styles.tabBarLabelUnderline : styles.tabBarLabel
            }
          >
            Moves
          </Text>
        </TouchableOpacity>
      </View>
      {index === 0 && (
        <View style={styles.tabContentWrap}>
          <AboutRow data={data} />
        </View>
      )}

      {index === 1 && (
        <View style={styles.tabContentWrap}>
          <StatsRow data={data?.stats} />
        </View>
      )}

      {index === 2 && (
        <View style={styles.tabContentWrap}>
          <EvolutionRow species={data?.species} />
        </View>
      )}

      {index === 3 && (
        <View style={styles.tabContentWrap}>
          <MovesRow data={data?.moves} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  tabStatusBar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    marginRight: 16,
    marginLeft: 16,
    marginBottom: 18,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(158, 150, 150, .2)",
  },
  tabBarLabel: {
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    lineHeight: 19,
    color: theme.colors.text,
    paddingBottom: 18,
    opacity: 0.5,
  },

  tabBarLabelUnderline: {
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    lineHeight: 19,
    color: theme.colors.text,
    opacity: 1,
    paddingBottom: 18,
    borderBottomWidth: 2,
    borderBottomColor: theme.colors.active,
  },
  tabContentWrap: {
    display: "flex",
    flexDirection: "column",
  },
});
