import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  ActivityIndicator,
  ImageBackground,
} from "react-native";
import { Pokemon, useGetPokemonByNameQuery } from "../../api";
import { theme } from "../../consts";
import { getPokemonImage } from "../../utils";
import { Pill } from "../pill";

type CardProps = {
  pokemon: Pokemon;
  name: string;
  color: string;
};

export const Card: React.FC<CardProps> = ({ pokemon, name, color }) => {
  const {
    data: pokemonDetails,
    isFetching,
    isError,
    error,
  } = useGetPokemonByNameQuery(name);

  const renderContent = () => {
    if (isFetching) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator />
        </View>
      );
    }

    if (isError) {
      return <Text>{error.toString()}</Text>;
    }

    return (
      <View
        style={{
          ...styles.cardWrap,
          backgroundColor: color,
        }}
      >
        <View style={styles.textWrap}>
          <Text style={styles.cardTitle}>{pokemon.name}</Text>
          {pokemonDetails?.types?.map((detail, i) => (
            <Pill key={i} type={detail?.type} />
          ))}
        </View>
        <ImageBackground
          source={require("../../assets/back.png")}
          resizeMode="cover"
          style={styles.imageWrap}
        >
          <Image style={styles.image} source={getPokemonImage(pokemon.name)} />
        </ImageBackground>
      </View>
    );
  };

  return <>{renderContent()}</>;
};

const styles = StyleSheet.create({
  cardWrap: {
    height: "100%",
    width: "100%",
    borderRadius: theme.borderRadius.m,
    paddingTop: theme.spacing.l,
    paddingLeft: 14,
    display: "flex",
    flexDirection: "row",
    overflow: "hidden",
    position: "relative",
  },
  cardTitle: {
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.fontSize.m,
    lineHeight: 19,
    letterSpacing: 0,
    marginBottom: theme.spacing.m,
    color: theme.colors.text_white,
    textTransform: "capitalize",
  },
  textWrap: {
    display: "flex",
    flexDirection: "column",
    marginRight: theme.spacing.s,
  },
  loader: {
    maxWidth: 186,
    height: 143,
    width: "100%",
    borderRadius: theme.borderRadius.m,
    paddingTop: theme.spacing.l,
    display: "flex",
    flexDirection: "row",
    marginBottom: theme.spacing.s,
    alignItems: "center",
    justifyContent: "center",
  },
  imageWrap: {
    width: 106,
    height: 106,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    left: 85,
    top: 37,
  },
  image: {
    width: 88,
    height: 80,
  },
});
