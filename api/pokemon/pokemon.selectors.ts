import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../store";

import { pokemonApi } from "./pokemon.api";
import { Pokemon } from "./pokemon.types";

export const selectPokemonResult = pokemonApi.endpoints.getAllPokemons.select();

export const selectPokemonDetails = (state: RootState, name: string) =>
  pokemonApi.endpoints.getPokemonByName.select(name)(state)?.data ?? {};

const emptyPokemons: Pokemon[] = [];

export const selectAllPokemons = createSelector(
  selectPokemonResult,
  (pokemonResult) => pokemonResult?.data ?? emptyPokemons
);
