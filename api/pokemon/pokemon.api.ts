import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { getPokemonImage } from "../../utils";
import {
  Pokemon,
  PokemonDetails,
  PokemonResponse,
  PokemonSpiecesResponse,
} from "./pokemon.types";

// Define a service using a base URL and expected endpoints
export const pokemonApi = createApi({
  reducerPath: "pokemonApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://pokeapi.co/api/v2",
  }),
  tagTypes: ["Pokemons"],
  endpoints: (builder) => ({
    getAllPokemons: builder.query<PokemonResponse, number>({
      query(page) {
        return `pokemon?offset=${page}&limit=20`;
      },
      providesTags: ["Pokemons"],
      transformResponse: (
        response: { results: PokemonResponse },
        meta,
        arg
      ) => {
        const extendedResponse = response.results.map((obj: Pokemon) => ({
          ...obj,
          image: getPokemonImage(obj.name),
        }));

        return extendedResponse;
      },
    }),
    getPokemonByName: builder.query<PokemonDetails, string>({
      query: (name) => {
        return `pokemon/${name}`;
      },
      async onQueryStarted({}, { dispatch, queryFulfilled }) {
        try {
          const { data: pokemonDetails } = await queryFulfilled;
          const patchResult = dispatch(
            pokemonApi.util.updateQueryData("getAllPokemons", 0, (draft) => {
              const results = draft.filter(
                (entry) => entry.name === pokemonDetails?.name
              );
              const type = {
                types: pokemonDetails.types,
              };
              Object.assign(results[0], type);
            })
          );
        } catch {}
      },
    }),
    getPokemonSpieces: builder.query<PokemonSpiecesResponse, string>({
      query: (name) => {
        return `pokemon-species/${name}`;
      },
    }),
  }),
});

export const {
  useGetAllPokemonsQuery,
  useGetPokemonByNameQuery,
  useGetPokemonSpiecesQuery,
} = pokemonApi;
