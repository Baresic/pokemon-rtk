import { NativeStackScreenProps } from "@react-navigation/native-stack";
import type { RouteProp } from "@react-navigation/native";

export type HomeStackNavigatorParamList = {
  Home: undefined;
  Details: {
    name: string;
    color?: {
      name: string;
      bg: string;
      value: string;
    };
  };
  Wishlist: undefined;
  Account: undefined;
};

export type HomeScreenStackNavigatorParamList = {
  HomeScreen: undefined;
  DetailsScreen: {
    name: string;
    color?: {
      name: string;
      bg: string;
      value: string;
    };
  };
};

export type HomeScreenStack = NativeStackScreenProps<
  HomeScreenStackNavigatorParamList,
  "HomeScreen",
  "DetailsScreen"
>;

export type DetailsScreenRouteProp = RouteProp<
  HomeStackNavigatorParamList,
  "Details"
>;
