import { useEffect, useState } from "react";
import {
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  useWindowDimensions,
} from "react-native";
import { Pokemon, useGetAllPokemonsQuery } from "../../api";
import { FilterMenu } from "../../components";
import { Card } from "../../components/card";
import { Hamburger } from "../../components/hamburger";
import { colorObjects, theme } from "../../consts";
import { HomeScreenStack } from "../../types";

export const HomeScreen = ({ navigation }: HomeScreenStack) => {
  const [pokemonList, setPokemonList] = useState<Pokemon[]>([]);
  const [isFilterLoading, setFilterLoading] = useState<boolean>(false);
  const {
    data: pokemons,
    isFetching,
    isSuccess,
    isError,
    error,
  } = useGetAllPokemonsQuery(0);

  const windowWidth = useWindowDimensions().width;

  const renderContent = () => {
    if (isError) {
      return <Text>{error.toString()}</Text>;
    }
    return pokemonList?.map((pokemon, i) => {
      return (
        <TouchableOpacity
          style={{ ...styles.cardwrap, width: windowWidth > 400 ? 186 : 172 }}
          onPress={() =>
            navigation.navigate("DetailsScreen", {
              name: pokemon.name,
              color: colorObjects[i],
            })
          }
          key={pokemon.url}
        >
          <Card
            color={colorObjects[i].value}
            pokemon={pokemon}
            name={pokemon.name}
          />
        </TouchableOpacity>
      );
    });
  };

  const loaderHelper = () => {
    setTimeout(() => {
      setFilterLoading(false);
    }, 300);
  };

  const filterByType = (text: string) => {
    const newPokemonlist = (pokemons as Pokemon[]).filter((item) => {
      return item?.types?.[0].type?.name === text;
    });
    setFilterLoading(true);
    setPokemonList(newPokemonlist);
    loaderHelper();
  };

  const clearFilters = () => {
    setFilterLoading(true);
    setPokemonList(pokemons as Pokemon[]);
    loaderHelper();
  };

  useEffect(() => {
    if (isSuccess) {
      setPokemonList(pokemons);
    }
  }, [isSuccess]);

  if (isFetching || isFilterLoading) {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: theme.colors.text_white,
          position: "relative",
        }}
      >
        <View style={styles.loader}>
          <ActivityIndicator />
        </View>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.colors.text_white,
        position: "relative",
      }}
    >
      <Hamburger navigation={navigation} />
      <ScrollView
        style={{
          width: "100%",
          paddingLeft: windowWidth > 400 ? 11 : 3,
          paddingRight: windowWidth > 400 ? 11 : 3,
          position: "relative",
          marginTop: 50,
        }}
        showsVerticalScrollIndicator={false}
      >
        {isSuccess && <Text style={styles.title}>Pokedex</Text>}
        <View style={styles.wrap}>{renderContent()}</View>
      </ScrollView>
      <View style={styles.filterMenu}>
        <FilterMenu filterBy={filterByType} clearFilters={clearFilters} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  wrap: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "flex-start",
    maxWidth: 402,
    marginLeft: "auto",
    marginRight: "auto",
    flex: 1,
  },
  cardwrap: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 186,
    height: 143,
    marginBottom: theme.spacing.s,
    marginLeft: 5,
    marginRight: 5,
  },
  loader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  title: {
    marginBottom: theme.spacing.s,
    color: theme.colors.text,
    fontSize: 24,
    fontFamily: theme.textVariants.body.fontFamily,
    marginLeft: 5,
  },
  filterMenu: {
    position: "absolute",
    bottom: 16,
    right: 16,
    zIndex: 1,
    backgroundColor: theme.colors.active,
    width: 56,
    height: 56,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: theme.borderRadius.xl,
  },
});
