import {
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  ImageBackground,
} from "react-native";
import { useSelector } from "react-redux";
import SearchBar from "@nghinv/react-native-search-bar";
import { PokemonDetails } from "../../api";
import { WishlistItem } from "../../components/wishlistItem";
import { theme } from "../../consts";
import { RootState } from "../../store";
import { useCallback, useEffect, useState } from "react";
import { AntDesign, MaterialIcons } from "@expo/vector-icons";
import { useFocusEffect } from "@react-navigation/native";

export const WishlistScreen = () => {
  const state = useSelector((state: RootState) => state);
  const [text, setText] = useState<string>("");

  const [wishList, setWishList] = useState<PokemonDetails[]>([]);

  const onChangeText = useCallback((value: string) => {
    setText(value);
  }, []);

  const find = (text: string) => {
    let textArray = text.split(" ");
    const newWishlist = wishList.filter((item) => {
      return textArray.every((el) => {
        return item.name.includes(el) || item.id === Number(el);
      });
    });
    setWishList(newWishlist);
  };

  const renderWishListItems = () => {
    if (state?.wishlist.pokemons.length === 0) {
      return <Text style={styles.empty}>Wishlist empty</Text>;
    }
    if (wishList.length === 0) {
      return <Text style={styles.empty}>No results</Text>;
    }
    return wishList?.map((pokemon: PokemonDetails) => {
      return (
        <WishlistItem key={pokemon.id} pokemon={pokemon as PokemonDetails} />
      );
    });
  };

  useEffect(() => {
    if (text.length > 0) {
      find(text);
    }
  }, [text]);

  useEffect(() => {
    if (text.length === 0) {
      setWishList(state?.wishlist.pokemons);
    }
  }, [text, state?.wishlist.pokemons]);

  useFocusEffect(
    useCallback(() => {
      setWishList(state?.wishlist.pokemons);

      return () => setText("");
    }, [state?.wishlist.pokemons])
  );

  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.colors.text_white,
      }}
    >
      <ScrollView
        style={{ width: "100%", paddingLeft: 16, paddingRight: 16 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.background}>
          <ImageBackground
            source={require("../../assets/back.png")}
            resizeMode="contain"
            style={styles.image}
          ></ImageBackground>
        </View>
        <Text style={styles.title}>Pokedex</Text>
        <Text style={styles.subTitle}>
          Search for Pokémon by name or by using the National Pokédex number.
        </Text>
        <View>
          <SearchBar
            placeholder="What pokemon you are looking for?"
            containerStyle={styles.textInput}
            borderRadius={50}
            height={58}
            value={text}
            onChangeText={onChangeText}
            cancelButton={false}
            clearIcon={
              <AntDesign
                name="close"
                size={24}
                color="black"
                onPress={() => {
                  setText("");
                }}
              />
            }
            searchIcon={
              <MaterialIcons
                name="search"
                size={24}
                color="black"
                style={{
                  paddingHorizontal: 8,
                }}
              />
            }
            textInputStyle={{
              fontFamily: theme.textVariants.body.fontFamily,
              fontSize: 14,
              paddingHorizontal: 16,
            }}
          />
        </View>
        {renderWishListItems()}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  background: {
    display: "flex",
    height: 406,
    width: "100%",
    position: "absolute",
    top: -238,
  },
  image: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    marginBottom: theme.spacing.s,
    marginTop: 70,
    color: theme.colors.text,
    fontSize: 24,
    lineHeight: 28,
    fontFamily: theme.textVariants.body.fontFamily,
    zIndex: 1,
  },
  subTitle: {
    lineHeight: 19,
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
    marginBottom: 30,
    zIndex: 1,
  },
  empty: {
    marginTop: 100,
    marginLeft: "auto",
    marginRight: "auto",
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
  },
  textInput: {
    marginBottom: theme.spacing.l,
  },
});
