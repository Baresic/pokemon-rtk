import { Entypo, Ionicons } from "@expo/vector-icons";
import { useRoute } from "@react-navigation/native";
import {
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  Image,
} from "react-native";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { PokemonDetails, useGetPokemonByNameQuery } from "../../api";
import { DetailsTabs, Pill } from "../../components";
import { theme } from "../../consts";
import { RootState } from "../../store";
import { pokemnsAdded, pokemonsDeleted } from "../../store/store/wishlist";
import { DetailsScreenRouteProp } from "../../types";
import { formatId, getPokemonImage } from "../../utils";

export const DetailsScreen = ({ navigation }: any) => {
  const route = useRoute<DetailsScreenRouteProp>();
  const wishlistItems = useSelector(
    (state: RootState) => state.wishlist.pokemons
  );
  const dispatch = useDispatch();
  const { data, isLoading } = useGetPokemonByNameQuery(route.params?.name);

  if (isLoading) {
    return (
      <View style={styles.loader}>
        <ActivityIndicator />
      </View>
    );
  }

  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.colors.text_white,
      }}
    >
      <ScrollView showsVerticalScrollIndicator={false} style={styles.view}>
        <View
          style={{
            ...styles.topPart,
            backgroundColor: route.params?.color?.value,
          }}
        >
          <View style={styles.backHolder}>
            <Ionicons
              name="ios-arrow-back-sharp"
              size={24}
              color={theme.colors.text_white}
              onPress={() => {
                console.warn("TETS");
                navigation.navigate("HomeScreen");
              }}
            />
            <Entypo
              name={
                wishlistItems.some((item) => item.name === data?.name)
                  ? "heart"
                  : "heart-outlined"
              }
              color="white"
              size={24}
              onPress={() => {
                wishlistItems.some((item) => item.name === data?.name)
                  ? dispatch(pokemonsDeleted(data?.name as string))
                  : dispatch(
                      pokemnsAdded({
                        ...(data as PokemonDetails),
                        bgColor: route.params?.color?.value,
                      })
                    );
              }}
            />
          </View>

          <View style={styles.nameHolder}>
            <Text style={styles.title}>{data?.name}</Text>
            <Text style={styles.id}>{formatId(data?.id)}</Text>
          </View>

          <View style={styles.nameHolderTwo}>
            <View style={styles.typeHolder}>
              {data?.types?.map((detail, i) => (
                <Pill key={i} type={detail?.type} isMargin />
              ))}
            </View>
            <Text style={styles.spieces}>{data?.species?.name}</Text>
          </View>

          <View
            style={{
              ...styles.imageCircle,
              backgroundColor: route.params?.color?.bg,
            }}
          >
            <Image
              style={styles.image}
              source={getPokemonImage(route.params?.name)}
            />
          </View>
          <DetailsTabs data={data as PokemonDetails} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  wrap: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "space-around",
    maxWidth: 392,
  },
  loader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  view: {
    width: "100%",
  },
  topPart: {
    height: 418,
    width: "100%",
  },
  backHolder: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    width: "100%",
    marginTop: theme.spacing.l,
    paddingLeft: theme.spacing.m,
    paddingRight: theme.spacing.m,
  },
  nameHolder: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    width: "100%",
    marginTop: 50,
    paddingLeft: theme.spacing.m,
    paddingRight: theme.spacing.m,
  },
  title: {
    fontSize: theme.fontSize.xl,
    lineHeight: 34,
    fontFamily: theme.textVariants.body.fontFamily,
    color: theme.colors.text_white,
  },
  id: {
    fontSize: 18,
    lineHeight: 22,
    fontFamily: theme.textVariants.body.fontFamily,
    color: theme.colors.text_white,
  },
  nameHolderTwo: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    width: "100%",
    marginTop: theme.spacing.m,
    paddingRight: theme.spacing.m,
  },
  spieces: {
    fontSize: theme.fontSize.m,
    lineHeight: 19,
    fontFamily: theme.textVariants.body.fontFamily,
    color: theme.colors.text_white,
  },
  typeHolder: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    paddingLeft: theme.spacing.m,
    paddingRight: theme.spacing.m,
  },
  imageCircle: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: 239,
    height: 239,
    borderRadius: theme.borderRadius.xl,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 45,
  },
  image: {
    width: 120,
    height: 120,
  },
});
