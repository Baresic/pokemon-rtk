import {
  Text,
  View,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
  StyleSheet,
} from "react-native";
import { theme } from "../../consts";

export const AccountScreen = () => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.colors.text_white,
      }}
    >
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          width: "100%",
          paddingLeft: 16,
          paddingRight: 16,
          marginTop: 85,
        }}
      >
        <View style={styles.infoHolder}>
          <Text style={styles.infoHolderText}>Full name:</Text>
          <Text style={styles.infoHolderText}>Ivan Baresic</Text>
        </View>
        <View style={styles.infoHolder}>
          <Text style={styles.infoHolderText}>Gender:</Text>
          <Text style={styles.infoHolderText}>M</Text>
        </View>
        <View style={styles.infoHolder}>
          <Text style={styles.infoHolderText}>Birthday:</Text>
          <Text style={styles.infoHolderText}>19/7/1983</Text>
        </View>
        <View style={styles.infoHolder}>
          <Text style={styles.infoHolderText}>Adress:</Text>
          <Text style={styles.infoHolderText}>Citizen of the world</Text>
        </View>
        <View style={styles.infoHolder}>
          <Text style={styles.infoHolderText}>Occupation:</Text>
          <Text style={styles.infoHolderText}>Fe dev</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  infoHolder: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    marginBottom: 16,
  },
  infoHolderText: {
    fontFamily: theme.textVariants.body.fontFamily,
    fontSize: theme.textVariants.body.fontSize,
  },
});
